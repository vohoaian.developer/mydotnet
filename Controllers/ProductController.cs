using Microsoft.AspNetCore.Mvc;
using PartyInvites.Models;

namespace PartyInvites.Controllers
{
  public class ProductController : Controller
  {
    public ViewResult Index()
    {
      var products = Product.GetProducts().Where(p => p != null);
      return View(products);
    }
  }
}